#include "controller.h"

int main(int argc, char** argv)
{
    Controller controller;
    controller.CheckArgs(argc, argv);
    controller.NavigationEditMode();
    return 0;
}

