#include "model.h"

Model::Model()
{
    SearchToEnd = true;
    exit = false;
    FirstShowLine = 0;
    FirstShowPos = 0;
    CurLine = 0;
    CurPos = 0;
    mode = 0;
}

void Model::do_command()
{
    if(command.empty())
        return;
    if(std::strcmp(command.c_str(), "q!") == 0)
        exit = true;
    else if(std::strcmp(command.c_str(), "q") == 0)
    {
        if(!file.modified)
            exit = true;
    }
    else if(std::strcmp(command.c_str(), "x") == 0 || std::strcmp(command.c_str(), "wq!") == 0)
    {
        file.save();
        exit = true;
    }
    else if(command[0] == 'w')
    {
        if(command.size() > 1)
        {
            command.erase(0,2);
            file.name = command;
            file.modified = true;
            file.save();
        }
        else
            file.save();
    }
    else if(command[0] == 'o')
    {
        MyString PrevName = file.name;
        command.erase(0,2);
        file.name = command;
        if(!file.open())
            file.name = PrevName;
    }
    else if(std::strcmp(command.c_str(), "h") == 0)
    {
        view.help();
        getch();
    }
    else
    {
        int i = 0;
        while (i < command.size() && command[i] > '0' && command[i] < '9')
            i++;
        if(i == command.size())
            GoToLineByNumber(command);
    }
}

void Model::CursorLeft()
{
    if(CurLine == 0 && CurPos == 0)
        return;
    if(CurPos == 0)
    {
        CurLine--;
        CurPos = file.strings[CurLine].size();
    }
    else
        CurPos--;
}

void Model::CursorRight()
{
    if(CurLine == file.strings.size() - 1 && CurPos == file.strings[CurLine].size())
        return;
    if(CurPos == file.strings[CurLine].size())
    {
        CurLine++;
        CurPos = 0;
    }
    else
        CurPos++;
}

void Model::CursorUp()
{
    if(CurLine == 0)
        return;
    CurLine--;
    if(CurPos >= file.strings[CurLine].size())
        CurPos = file.strings[CurLine].size();
}

void Model::CursorDown()
{
    if(CurLine == file.strings.size() - 1)
        return;
    CurLine++;
    if(CurPos >= file.strings[CurLine].size())
        CurPos = file.strings[CurLine].size();
}

void Model::CursorStartString()
{
    CurPos = 0;
}

void Model::CursorEndString()
{
    CurPos = file.strings[CurLine].size();
}

void Model::CursorStartFile()
{
    CurLine = 0;
    CurPos = 0;
}

void Model::CursorEndFile()
{
    CurLine = file.strings.size() - 1;
    CurPos = file.strings[CurLine].size();
}

void Model::CursorStartWord()
{
    MyString CurString = file.strings[CurLine];
    while(CurPos > 0 && CurString[CurPos-1] == ' ')
        CursorLeft();
    while(CurPos > 0  && CurString[CurPos-1] != ' ')
        CursorLeft();
}

void Model::CursorEndWord()
{
    MyString CurString = file.strings[CurLine];
    while(CurPos < CurString.size() && CurString[CurPos] == ' ')
        CursorRight();
    while(CurPos < CurString.size() && CurString[CurPos] != ' ')
        CursorRight();
}

void Model::PageUp()
{
    FirstShowLine -= view.MaxLine - 2;
    if(FirstShowLine < 0)
        FirstShowLine = 0;
    CurLine = FirstShowLine;
    CurPos = 0;
}

void Model::PageDown()
{
    int max = file.strings.size() - view.MaxLine + 2;
    FirstShowLine += view.MaxLine - 2;
    if(max > 0 && FirstShowLine > max)
        FirstShowLine = max;
    else if(max < 0 && FirstShowLine > max)
        FirstShowLine -= view.MaxLine - 2;
    CurLine = FirstShowLine;
    CurPos = 0;
}

void Model::GoToLineByNumber(MyString number)
{
    int LineNum = 0, step = 1, num;
    while(!number.empty())
    {
        num = number[number.size() - 1] - '0';
        LineNum += num*step;
        step *= 10;
        number.erase(number.size() - 1, 1);
    }
    if(LineNum > file.strings.size())
        return;
    CurLine = LineNum - 1;
    CurPos = 0;
}

void Model::backspace()
{
    if(CurLine == 0 && CurPos == 0)
        return;
    if(CurPos != 0)
    {
        file.strings[CurLine].erase(CurPos - 1, 1);
        CursorLeft();
    }
    else
    {
        CursorLeft();
        file.strings[CurLine] = file.strings[CurLine] + file.strings[CurLine + 1];
        auto i = file.strings.begin();
        std::advance(i, CurLine+1);
        file.strings.erase(i);
    }
    file.modified = true;
}

void Model::insert(int symbol)
{
    if(symbol == '\n')
    {
        MyString NewString = file.strings[CurLine].substr(CurPos);
        if(!NewString.empty())
            file.strings[CurLine].erase(CurPos, NewString.size());
        auto i = file.strings.begin();
        std::advance(i, CurLine+1);
        file.strings.insert(i, NewString);
    }
    else
        file.strings[CurLine].insert(CurPos, 1, symbol);
    CursorRight();
    file.modified = true;
}

void Model::DeleteString()
{
    file.strings[CurLine].clear();
    CursorStartString();
    file.modified = true;
}

void Model::DeleteWord()
{
    if(CurPos >= file.strings[CurLine].size() || file.strings[CurLine][CurPos] < 'A' || file.strings[CurLine][CurPos] > 'z')
        return;
    while(CurPos > 0 && file.strings[CurLine][CurPos-1] != ' ')
        backspace();
    while(CurPos < file.strings[CurLine].size() && !file.strings[CurLine].empty() && file.strings[CurLine][CurPos] != ' ')
        DeleteAfterCursor();
    while(CurPos < file.strings[CurLine].size() && !file.strings[CurLine].empty() && file.strings[CurLine][CurPos] == ' ')
        DeleteAfterCursor();
}

void Model::ChangeSymbol(int symbol)
{
    CursorRight();
    backspace();
    insert(symbol);
    CursorLeft();
}

void Model::DeleteAfterCursor()
{
    if(CurLine == file.strings.size() - 1 && CurPos >= file.strings[CurLine].size())
        return;
    CursorRight();
    backspace();
}

void Model::CopyString()
{
    buffer = file.strings[CurLine];
}

void Model::CopyWord()
{
    MyString CurString = file.strings[CurLine];
    if(CurPos >= CurString.size() || CurString[CurPos] < 'A' || CurString[CurPos] > 'z')
        return;
    unsigned int PosStart = CurPos, PosEnd;
    while(PosStart - 1 > 0 && CurString[PosStart - 1] != ' ')
        PosStart--;
    PosEnd = PosStart;
    while(PosEnd < CurString.size() && CurString[PosEnd] != ' ')
        PosEnd++;
    buffer = CurString.substr(PosStart, PosEnd-PosStart);
}

void Model::cut()
{
    CopyString();
    DeleteString();
}

void Model::paste()
{
    unsigned int i;
    for(i=0; i < buffer.size(); i++)
        insert(buffer[i]);
}

void Model::ChangeSearchDir()
{
    if(SearchToEnd)
        SearchToEnd = false;
    else
        SearchToEnd = true;
}

void Model::MakeSearch()
{
    int line = CurLine, pos;
    int last;
    if(SearchToEnd)
    {
        line++;
        pos = CurPos + 1;
        if(pos + search.size() <= file.strings[CurLine]. size())
        {
            pos = file.strings[CurLine].find(search.c_str(), pos);
            if (pos != -1 && pos < file.strings[CurLine].size())
            {
                CurPos = pos;
                return;
            }
        }
        for(line; line < file.strings.size(); line++)
        {
            if(file.strings[line].size() >= search.size())
            {
                pos = file.strings[line].find(search.c_str());
                if (pos != -1)
                {
                    CurPos = pos;
                    CurLine = line;
                    return;
                }
            }
        }
    }
    else
    {
        last = -1;
        while(true)
        {
            if(file.strings[CurLine].size() > last + search.size())
                pos = file.strings[CurLine].find(search.c_str(), last + 1);
            if(pos != -1 && pos < CurPos)
                last = pos;
            else
                break;
        }
        if(last >= 0)
        {
            CurPos = last;
            return;
        }
        for(line; line > 0; line--)
        {
            if(file.strings[line - 1].size() > last + search.size())
            {
                while (true)
                {
                    pos = file.strings[line - 1].find(search.c_str(), last + 1);
                    if (pos != -1 && pos < file.strings[line-1].size())
                        last = pos;
                    else
                        break;
                }
                if (last >= 0)
                {
                    CurPos = last;
                    CurLine = line - 1;
                    return;
                }
            }
        }
    }
}

void Model::output()
{
    MyString CurString;
    unsigned int i, j;
    clear();
    view.UpdateBorders();
    if(CurLine < FirstShowLine)
        FirstShowLine = CurLine;
    else if(CurLine >= FirstShowLine + view.MaxLine - 3)
        FirstShowLine += CurLine - (FirstShowLine + view.MaxLine - 3);
    if(CurPos < FirstShowPos)
        FirstShowPos = CurPos;
    else if(CurPos >= FirstShowPos + view.MaxPos - 1)
        FirstShowPos += CurPos - (FirstShowPos + view.MaxPos - 1);
    for(i = FirstShowLine; i < file.strings.size() && i < view.MaxLine - 2 + FirstShowLine; i++)
    {
        CurString = file.strings[i];
        for(j = FirstShowPos; j < CurString.size() && j < view.MaxPos - 1 + FirstShowPos; j++)
            view.SymbolPrint(CurString[j]);
        view.SymbolPrint('\n');
    }
    view.status(mode, command, search, CurLine+1, file.strings.size(), CurPos+1, file.ShortName);
    view.CursorMove(CurLine - FirstShowLine, CurPos - FirstShowPos);
    refresh();
}
