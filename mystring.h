#ifndef _MYSTRING_H_ 
#define _MYSTRING_H_ 

#include <cstring>
#include <ostream>
#include <istream>

class MyString
{
public:
	MyString();                                     // default constructor
	MyString(const MyString& str);                  // copy constructor
	MyString(const char* str);                      // char array constructor
	MyString(std::initializer_list<char> list);     // initializer list constructor
	MyString(std::string& str);                     // std::string constructor
	MyString(const char* str, size_t count);        // initialize class with count characters of �char string�
	MyString(size_t count, char sym);               // initialize class with count of characters

	~MyString();

	MyString operator +(const MyString& str);       // concatenate with MyString
	MyString operator +(const char* str);           // concatenate with char array
	MyString operator +(const std::string& str);    // concatenate with std::string

	MyString& operator =(const MyString& str);      // MyString assignment
	MyString& operator =(const char* str);          // char array assignment
	MyString& operator =(const std::string& str);   // std::string assignment
	MyString& operator =(char sym);                 // char assignment

	MyString& operator+= (const MyString& str);     // assignment concatenate with Mystring
	MyString& operator+= (const char* str);         // assignment concatenate with char array
	MyString& operator+= (const std::string& str);  // assignment concatenate with std::string

	bool operator> (const MyString& str) const;     // lexicographically comparing
	bool operator< (const MyString& str) const;
	bool operator<= (const MyString& str) const;
	bool operator>= (const MyString& str) const;
	bool operator== (const MyString& str) const;
	bool operator!= (const MyString& str) const;

	char operator[] (unsigned int i)  const;              // index operator

	friend std::ostream& operator<< (std::ostream& os, MyString& str);
	friend std::istream& operator>> (std::istream& is, MyString& str);

	char* c_str() const;                       // return a pointer to null-terminated character array
	char* data() const;                        // return a pointer to array data that not required to be null-terminated
	size_t length() const;                     // same as size
	size_t size() const;                       // return the number of char elements in string
	bool empty() const;                        // true if string is empty
	size_t capacity() const;                   // return the current amount of allocated memory for array
	void shrink_to_fit();                      // reduce the capacity to size
	void clear();                              // remove all char element in string

	void insert(unsigned int i, size_t count, char sym);               // insert count of char in index position
	void insert(unsigned int i, const char* str);                      // insert null-terminated char string at index position
	void insert(unsigned int i, const char* str, size_t count);        // insert count of null-terminated char string at index position
	void insert(unsigned int i, const std::string& str);               // insert std::string at index position
	void insert(unsigned int i, const std::string& str, size_t count); // insert count of std::string at index position

	void erase(unsigned int i, size_t count);                          // erase count of char at index position

	void append(size_t count, char sym);                               // append count of char
	void append(const char* str);                                      // append null-terminated char string
	void append(const char* str, unsigned int i, size_t count);        // append a count of null-terminated char string by index position
	void append(const std::string& str);                               // append std:: string
	void append(const std::string& str, unsigned int i, size_t count); // append a count of std:: string by index position

	void replace(unsigned int i, size_t count, const char* str);        // replace a count of char at index by �string�
	void replace(unsigned int i, size_t count, const std::string& str); // replace a count of char at index by std::string

	MyString substr(unsigned int i);                                     //return a substring starts with index position
	MyString substr(unsigned int i, size_t count);                       // return a count of substring�s char starts with index position

	unsigned int find(const char* str) const;                        // if founded return the index of substring
	unsigned int find(const char* str, unsigned int i) const;        // same as find(�string�) but search starts from index position
	unsigned int find(const std::string& str) const;                 // if founded return the index of substring
	unsigned int find(const std::string& str, unsigned int i) const; // same as find(std::string) but search starts from index position

	void MyNewStr(size_t size);                                      //creates MyString str with the size of 'size'

	//Return the index of substring if founded, search starts from index (i) position
	unsigned int MyFind(const char* str, unsigned int i) const;

private:
	char* str;
	size_t _size;
	size_t _capacity;
};

std::ostream& operator<< (std::ostream& os, MyString& str);
std::istream& operator>> (std::istream& is, MyString& str);

#endif //STRING_MYSTRING_H