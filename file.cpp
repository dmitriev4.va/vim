#include "file.h"

File::File()
{
    modified = false;
}

int File::open()
{
    MyString string;
    char sym;
    std::ifstream file;
    file.open(name.c_str());
    if(!file && name == "new.txt")
    {
        strings.push_back(string);
        shortname();
        return 1;
    }
    else if(!file)
        return 0;
    strings.clear();
    while(true)
    {
        sym = file.get();
        if(sym == '\n')
        {
            strings.push_back(string);
            string.clear();
        }
        else if(sym == -1)
            break;
        else
            string.append(1,sym);
    }
    file.close();
    modified = false;
    if(strings.empty())
        strings.push_back(string);
    shortname();
    return 1;
}

void File::save()
{
    unsigned int i;
    std::ofstream file;
    file.open(name.c_str());
    for(i = 0; i < strings.size(); i++)
        file << strings[i] << "\n";
    modified = false;
    shortname();
}

void File::shortname()
{
    ShortName.clear();
    int i = name.size() - 1;
    while( i>= 0 && name[i] != '/')
    {
        ShortName.insert(0,1,name[i]);
        i--;
    }
}
