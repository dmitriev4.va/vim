#include "view.h"

View::View()
{
    initscr();
    noecho();
    keypad(stdscr, true);
    use_default_colors();
    start_color();
    init_pair(1, COLOR_WHITE, -1);
    init_pair(2, COLOR_RED, -1);
    attron(COLOR_PAIR(1));
    getmaxyx(stdscr, MaxLine, MaxPos);
}

View::~View()
{
    attroff(COLOR_PAIR(1));
    endwin();
}

void View::status(int mode, MyString command, MyString search, int CurLine, int LinesNum , int CurPos, MyString FileName)
{
    attron(COLOR_PAIR(2));
    switch(mode)
    {
        case 0:
            mvprintw(MaxLine - 1, 0, "[navigation edit mode]     file: %s  line: %d/%d  column: %d", FileName.c_str(), CurLine, LinesNum, CurPos);
            break;
        case 1:
            mvprintw(MaxLine - 1, 0, "[insert mode]              file: %s  line: %d/%d  column: %d", FileName.c_str(), CurLine, LinesNum, CurPos);
            break;
        case 2:
            mvprintw(MaxLine - 1, 0, "[command mode]             file: %s  line: %d/%d  column: %d", FileName.c_str(), CurLine, LinesNum, CurPos);
            if(!command.empty())
                mvprintw(MaxLine - 2, 0, "$: %s", command.c_str());
            else
                mvprintw(MaxLine - 2, 0, "$: ");
            break;
        case 3:
            mvprintw(MaxLine - 1, 0, "[search mode]              file: %s  line: %d/%d  column: %d", FileName.c_str(), CurLine, LinesNum, CurPos);
            if(!search.empty())
                mvprintw(MaxLine - 2, 0, "Search for: %s", search.c_str());
            else
                mvprintw(MaxLine - 2, 0, "Search for: ");
            break;
        default:
            break;
    }
    attron(COLOR_PAIR(1));
}

void View::SymbolPrint(char symbol)
{
    addch(symbol);
}

void View::CursorMove(int line, int pos)
{
    move(line, pos);
}

void View::UpdateBorders()
{
    getmaxyx(stdscr, MaxLine, MaxPos);
}

void View::help() const
{
    clear();
    mvprintw(MaxLine - 1, 0, "[command mode]");
    mvprintw(0, 0, "o filename - opens file <filename>");
    mvprintw(1, 0, "x - save file and quit");
    mvprintw(2, 0, "w - save file");
    mvprintw(3, 0, "w filename - saves file <filename>");
    mvprintw(4, 0, "q - quit if file wasn't modified");
    mvprintw(5, 0, "q! - quit without saving");
    mvprintw(6, 0, "wq! - save file and quit");
    mvprintw(7, 0, "<number> - go to line by number");
    mvprintw(9, 0, "Press any button to continue");
}