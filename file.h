#ifndef VIM_FILE_H
#define VIM_FILE_H

#include "mystring.h"
#include <fstream>
#include <cstring>
#include <vector>

class File
{
public:
    File();
    int open();
    void save();

    void shortname();

    MyString name;
    MyString ShortName;
    bool modified;
    std::vector<MyString> strings;
};

#endif //VIM_FILE_H
