#ifndef VIM_CONTROLLER_H
#define VIM_CONTROLLER_H

#include <iostream>
#include "model.h"

class Controller
{
public:
    void CheckArgs(int argc, char** argv);

    void NavigationEditMode();
    void InsertMode();
    void CommandMode();
    void SearchMode(int symbol);

    Model model;
};
#endif //VIM_CONTROLLER_H
