#include "controller.h"

void Controller::CheckArgs(int argc, char** argv)
{
    if(argc > 1)
    {
        model.file.name = argv[1];
        if(!model.file.open())
        {
            model.file.name = "new.txt";
            model.file.open();
        }
    }
    else
    {
        model.file.name = "new.txt";
        model.file.open();
    }
}

void Controller::NavigationEditMode()
{
    int symbol;
    MyString number;
    model.output();
    while(true)
    {
        symbol = getch();
        switch(symbol)
        {
            case ':':
                CommandMode();
                if(model.exit)
                    return;
                break;
            case 'I':
                model.CursorStartString();
                InsertMode();
                break;
            case 'A':
                model.CursorEndString();
                InsertMode();
                break;
            case 'S':
                model.DeleteString();
                InsertMode();
                break;
            case 'i':
                InsertMode();
                break;
            case 'r':
                symbol = getch();
                model.ChangeSymbol(symbol);
                break;
            case 'p':
                model.paste();
                break;
            case '0': case '^': case KEY_HOME:
                model.CursorStartString();
                break;
            case '$': case KEY_END:
                model.CursorEndString();
                break;
            case KEY_LEFT:
                model.CursorLeft();
                break;
            case KEY_RIGHT:
                model.CursorRight();
                break;
            case KEY_UP:
                model.CursorUp();
                break;
            case KEY_DOWN:
                model.CursorDown();
                break;
            case 'w':
                model.CursorEndWord();
                break;
                case 'b':
                model.CursorStartWord();
                break;
            case 'G':
                model.CursorEndFile();
                break;
            case 'x':
                model.DeleteAfterCursor();
                break;
            case KEY_PPAGE:
                model.PageUp();
                break;
            case KEY_NPAGE:
                model.PageDown();
                break;
            case 'd':
                symbol = getch();
                switch (symbol)
                {
                    case 'd':
                        model.cut();
                        break;
                    case 'i':
                        symbol = getch();
                        if (symbol == 'w')
                            model.DeleteWord();
                        break;
                    default:
                        break;
                }
                break;
            case 'g':
                symbol = getch();
                if (symbol == 'g')
                    model.CursorStartFile();
                break;
            case 'y':
                symbol = getch();
                switch (symbol)
                {
                    case '\n':
                        model.CopyString();
                        break;
                    case 'w':
                        model.CopyWord();
                        break;
                    default:
                        break;
                }
                break;
            case '/': case '?':
                SearchMode(symbol);
                break;
            case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
                number.clear();
                number.append(1,symbol);
                while(true)
                {
                    symbol = getch();
                    if(symbol == 'G')
                    {
                        model.GoToLineByNumber(number);
                        break;
                    }
                    else if(symbol < '0' || symbol > '9')
                        break;
                    else
                        number.append(1,symbol);
                }
                break;
            default:
                break;
        }
        model.mode = 0;
        model.output();
    }
}

void Controller::InsertMode()
{
    int symbol;
    model.mode = 1;
    while(true)
    {
        model.output();
        symbol = getch();
        switch(symbol)
        {
            case KEY_LEFT:
                model.CursorLeft();
                break;
            case KEY_RIGHT:
                model.CursorRight();
                break;
            case KEY_UP:
                model.CursorUp();
                break;
            case KEY_DOWN:
                model.CursorDown();
                break;
            case KEY_BACKSPACE:
                model.backspace();
                break;
            case KEY_HOME:
                model.CursorStartString();
                break;
            case KEY_END:
                model.CursorEndString();
                break;
            case '\e':
                return;
            default:
                model.insert(symbol);
                break;
        }
    }
}

void Controller::CommandMode()
{
    model.command.clear();
    model.mode = 2;
    int symbol;
    while(true)
    {
        model.output();
        symbol = getch();
        switch(symbol)
        {
            case KEY_BACKSPACE:
                if(!model.command.empty())
                    model.command.erase(model.command.size()-1, 1);
                break;
            case '\n':
                model.do_command();
                model.command.clear();
                if(model.exit)
                    return;
                break;
            case '\e':
                return;
            default:
                if(model.command.size() < model.view.MaxPos - 3)
                    model.command.append(1, symbol);
                break;
        }
    }
}

void Controller::SearchMode(int mode)
{
    bool exit;
    model.mode = 3;
    if(mode == '/')
        model.SearchToEnd = true;
    else
        model.SearchToEnd = false;
    model.search.clear();
    int symbol;
    while(true)
    {
        model.output();
        symbol = getch();
        switch(symbol)
        {
            case KEY_BACKSPACE:
                if(!model.search.empty())
                    model.search.erase(model.search.size()-1, 1);
                break;
            case '\e':
                return;
            case '\n':
                model.MakeSearch();
                exit = false;
                while(!exit)
                {
                    model.output();
                    symbol = getch();
                    switch (symbol)
                    {
                        case '\e':
                            return;
                        case 'n':
                            model.MakeSearch();
                            break;
                        case 'N':
                            model.ChangeSearchDir();
                            model.MakeSearch();
                            model.ChangeSearchDir();
                            break;
                        case KEY_BACKSPACE:
                            exit = true;
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                if(model.search.size() < model.view.MaxPos - 12)
                    model.search.append(1, symbol);
                break;
        }
    }
}

