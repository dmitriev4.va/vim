#ifndef VIM_MODEL_H
#define VIM_MODEL_H

#include "view.h"

class Model
{
public:
    View view;
    MyString search;
    MyString command;
    MyString buffer;

    bool SearchToEnd;
    bool exit;
    File file;
    int FirstShowLine;
    int FirstShowPos;
    int CurLine;
    int CurPos;
    int mode;

    Model();

    void do_command();

    void CursorLeft();
    void CursorRight();
    void CursorUp();
    void CursorDown();
    void CursorStartString();
    void CursorEndString();
    void CursorStartFile();
    void CursorEndFile();
    void CursorStartWord();
    void CursorEndWord();

    void PageUp();
    void PageDown();

    void GoToLineByNumber(MyString number);

    void insert(int symbol);
    void backspace();

    void DeleteString();
    void DeleteWord();
    void ChangeSymbol(int symbol);
    void DeleteAfterCursor();

    void CopyString();
    void CopyWord();
    void cut();
    void paste();

    void ChangeSearchDir();
    void MakeSearch();

    void output();
};

#endif //VIM_MODEL_H
