#ifndef VIM_VIEW_H
#define VIM_VIEW_H

#include <ncurses.h>
#include "file.h"

class View
{
public:
    int MaxLine;
    int MaxPos;

    View();
    ~View();

    void status(int mode, MyString command, MyString search, int CurLine, int LinesNum, int CurPos, MyString FileName);
    void SymbolPrint(char symbol);
    void CursorMove(int line, int pos);
    void UpdateBorders();
    void help() const;
};

#endif //VIM_VIEW_H
